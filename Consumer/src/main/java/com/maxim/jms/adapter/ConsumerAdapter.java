package com.maxim.jms.adapter;

import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	
	public void sendToMongo(String json) throws UnknownHostException {
		logger.info("Sending to MongoDB");
		//Instantiate a Mongo Client
		MongoClient client = new MongoClient();
		//Get Database.  If not there, it creates for us
		@SuppressWarnings("deprecation")
		DB db = client.getDB("vendor");
		//Get Collection(table)
		DBCollection collection = db.getCollection("conact");
		logger.info("Converting JSON to DBObject");
		//Insert our entry (DB Object)
		DBObject object = (DBObject)JSON.parse(json);
		collection.insert(object);
		logger.info("Sent to MongoDB");
	}


}
